package com.nt.mishra.util;

public interface AppConstants {

	 public static String DEFAULT_PAGE_NUMBER = "0";
	 public static String DEFAULT_PAGE_SIZE = "30";

	    public static int MAX_PAGE_SIZE = 50;

}
